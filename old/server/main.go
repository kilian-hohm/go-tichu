package main

import (
	"gitlab.com/kilian-hohm/go-tichu/old/communication/server"
)

func main() {
	g := server.NewGameServer()
	g.Start()
}
