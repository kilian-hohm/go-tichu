package server

import (
	"fmt"
	"net/http"

	"gitlab.com/kilian-hohm/go-tichu/old/game"
)

// GameServer holds information for the websocket server
type GameServer struct {
	games map[string]game.Game
	port  int
}

// NewGameServer creates a new web server for a tichu game
func NewGameServer() GameServer {
	g := GameServer{port: 9000, games: map[string]game.Game{}}
	http.HandleFunc("/game/new", g.HandleNewGame)
	return g
}

// Start serves the server and listens for incoming requests
func (gs *GameServer) Start() {
	fmt.Printf("Starting game server and listening on port %d ...\n", gs.port)
	if err := http.ListenAndServe(fmt.Sprintf(":%d", gs.port), nil); err != nil {
		fmt.Printf("Server problem: %v\n", err)
	}
}

// HandleNewGame handles incoming requests to start a new game
func (gs *GameServer) HandleNewGame(res http.ResponseWriter, req *http.Request) {
	g := game.NewGame()
	gs.games[g.ID()] = g
	fmt.Printf("New game created: %s", g.ID())
	res.Write([]byte(g.ID()))
}
