package game

var deckMap map[int]Card

func init() {
	deckMap = map[int]Card{
		// red cards
		0:  Card{value: "2", family: "red", special: ""},
		1:  Card{value: "3", family: "red", special: ""},
		2:  Card{value: "4", family: "red", special: ""},
		3:  Card{value: "5", family: "red", special: ""},
		4:  Card{value: "6", family: "red", special: ""},
		5:  Card{value: "7", family: "red", special: ""},
		6:  Card{value: "8", family: "red", special: ""},
		7:  Card{value: "9", family: "red", special: ""},
		8:  Card{value: "10", family: "red", special: ""},
		9:  Card{value: "B", family: "red", special: ""},
		10: Card{value: "D", family: "red", special: ""},
		11: Card{value: "K", family: "red", special: ""},
		12: Card{value: "A", family: "red", special: ""},

		// green cards
		13: Card{value: "2", family: "green", special: ""},
		14: Card{value: "3", family: "green", special: ""},
		15: Card{value: "4", family: "green", special: ""},
		16: Card{value: "5", family: "green", special: ""},
		17: Card{value: "6", family: "green", special: ""},
		18: Card{value: "7", family: "green", special: ""},
		19: Card{value: "8", family: "green", special: ""},
		20: Card{value: "9", family: "green", special: ""},
		21: Card{value: "10", family: "green", special: ""},
		22: Card{value: "B", family: "green", special: ""},
		23: Card{value: "D", family: "green", special: ""},
		24: Card{value: "K", family: "green", special: ""},
		25: Card{value: "A", family: "green", special: ""},

		// blue cards
		26: Card{value: "2", family: "blue", special: ""},
		27: Card{value: "3", family: "blue", special: ""},
		28: Card{value: "4", family: "blue", special: ""},
		29: Card{value: "5", family: "blue", special: ""},
		30: Card{value: "6", family: "blue", special: ""},
		31: Card{value: "7", family: "blue", special: ""},
		32: Card{value: "8", family: "blue", special: ""},
		33: Card{value: "9", family: "blue", special: ""},
		34: Card{value: "10", family: "blue", special: ""},
		35: Card{value: "B", family: "blue", special: ""},
		36: Card{value: "D", family: "blue", special: ""},
		37: Card{value: "K", family: "blue", special: ""},
		38: Card{value: "A", family: "blue", special: ""},

		// yellow cards
		39: Card{value: "2", family: "yellow", special: ""},
		40: Card{value: "3", family: "yellow", special: ""},
		41: Card{value: "4", family: "yellow", special: ""},
		42: Card{value: "5", family: "yellow", special: ""},
		43: Card{value: "6", family: "yellow", special: ""},
		44: Card{value: "7", family: "yellow", special: ""},
		45: Card{value: "8", family: "yellow", special: ""},
		46: Card{value: "9", family: "yellow", special: ""},
		47: Card{value: "10", family: "yellow", special: ""},
		48: Card{value: "B", family: "yellow", special: ""},
		49: Card{value: "D", family: "yellow", special: ""},
		50: Card{value: "K", family: "yellow", special: ""},
		51: Card{value: "A", family: "yellow", special: ""},

		// special cards
		52: Card{value: "", family: "", special: "dragon"},
		53: Card{value: "", family: "", special: "phoenix"},
		54: Card{value: "", family: "", special: "dog"},
		55: Card{value: "", family: "", special: "dog"},
	}

}
