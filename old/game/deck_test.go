package game

import (
	"reflect"
	"testing"
)

func TestNewDeck(t *testing.T) {
	d := NewDeck()

	if len(d.cards) != len(deckMap) {
		t.Error("deck does not contain 56 cards")
	}

	for i := 0; i < len(deckMap); i++ {
		t.Log(d.cards[i])
		if d.cards[i] != deckMap[i] {
			t.Error("initial deck is not correct")
		}
	}
}

func TestShuffleDeck(t *testing.T) {
	d := NewDeck()

	d.Shuffle(0)

	if !reflect.DeepEqual(d.cards[0], Card{family: "blue", value: "3"}) {
		t.Error("Was not shuffled")
	}
}
