package game

import (
	"fmt"
	"math/rand"
)

// Card represents a playing card of tichu
type Card struct {
	family  string
	value   string
	special string
}

// Deck contains the 54 cards of a tichu game
type Deck struct {
	cards []Card
}

func (c Card) String() string {
	if c.special == "" {
		return fmt.Sprintf("%s %s", c.family, c.value)
	}
	return c.special

}

// NewDeck creates a new tichu deck
func NewDeck() Deck {
	d := Deck{}
	for i := 0; i < len(deckMap); i++ {
		d.cards = append(d.cards, deckMap[i])
	}
	return d
}

// Shuffle randomly shuffles the deck of cards
func (d *Deck) Shuffle(seed int64) {
	rand.Seed(seed)
	rand.Shuffle(len(d.cards), func(i, j int) {
		d.cards[i], d.cards[j] = d.cards[j], d.cards[i]
	})
}
