package game

import (
	"errors"
	"fmt"

	"github.com/google/uuid"
)

// ErrMaxPlayerReached error if no new player can be added to a game
var ErrMaxPlayerReached = errors.New("maximum number of players reached")

// Player holds information about a tichu player
type Player struct {
	id   uuid.UUID
	name string
}

// NewPlayer creates a new Player with the given name and a unique id
func NewPlayer(name string) Player {
	return Player{name: name, id: uuid.New()}
}

func (p Player) String() string {
	return fmt.Sprintf("%s (%s)", p.name, p.id)
}

// Game holds information about the tichu game
type Game struct {
	id      uuid.UUID
	nPlayer int
	players []Player
	deck    Deck
}

// ID returns the unique ID of the game as string
func (g Game) ID() string {
	return g.id.String()
}

// NewGame creates a new tichu game
func NewGame() Game {
	return Game{id: uuid.New(), deck: NewDeck(), nPlayer: 4}
}

// AddPlayer adds a player to the game if the number of players is not yet
// reached. If number of player is reached an error is returned.
func (g *Game) AddPlayer(p Player) error {
	if len(g.players) < g.nPlayer {
		g.players = append(g.players, p)
		return nil
	}
	return ErrMaxPlayerReached
}

// IsReady returns true if enough players where added
func (g Game) IsReady() bool {
	if len(g.players) == g.nPlayer {
		return true
	}
	return false
}
