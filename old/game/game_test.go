package game

import (
	"fmt"
	"testing"
)

func TestNewPlayer(t *testing.T) {
	p1 := NewPlayer("kathi")
	p2 := NewPlayer("kathi")

	if p1.name != "kathi" || p2.name != "kathi" {
		t.Error("name was not set correctly")
	}

	if p1.id == p2.id {
		t.Error("two players have the same name")
	}
}

func TestNewGame(t *testing.T) {
	g := NewGame()

	for i := 0; i < 4; i++ {
		err := g.AddPlayer(NewPlayer(fmt.Sprintf("p%d", i)))
		if err != nil {
			t.Errorf("prolbem adding player %d: %v", i, err)
		}
		if len(g.players) != i+1 {
			t.Errorf("player %d was not added", i)
		}
		if i < 3 && g.IsReady() {
			t.Errorf("game already ready after adding %d players", i+1)
		}
	}

	if !g.IsReady() {
		t.Errorf("game not ready allthough 4 players were added")
	}

	err := g.AddPlayer(NewPlayer("invalid player"))
	if err == nil {
		t.Errorf("could add more than 4 players: %v", err)
	}
	if len(g.players) != 4 {
		t.Errorf("player was falsly added")
	}
}
