package main

import (
	"fmt"

	"gitlab.com/kilian-hohm/go-tichu/old/game"
)

func main() {
	fmt.Print("What is your name: ")
	var name string
	fmt.Scanln(&name)
	p := game.NewPlayer(name)
	fmt.Println(p)
}
