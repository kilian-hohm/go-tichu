package clclient

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	"gitlab.com/kilian-hohm/go-tichu/internal/tichu/application"
	"gitlab.com/kilian-hohm/go-tichu/internal/tichu/model"
)

type CommandLineClient struct {
	reader          *bufio.Reader
	game            *application.TichuGame
	currentPlayerID int
	done            bool
}

func NewCommandLineClient() *CommandLineClient {
	fmt.Printf("%s\n\n", dragon)
	fmt.Printf("\tWelcome to a new game of Tichu\n\n")
	clc := CommandLineClient{reader: bufio.NewReader(os.Stdin)}
	clc.game = application.NewTichuGame(&clc)

	return &clc
}

func (clc *CommandLineClient) NextPrompt() bool {
	switch clc.game.GetPhase() {
	case application.DealingCards:
		clc.promptCardDeal()
	case application.PushingCards:
		clc.promptCardPush()
	case application.Trick:
		clc.promptTrickTurn()
	}

	if clc.done {
		return false
	}
	return true
}

func (clc *CommandLineClient) promptCardDeal() {
	clc.printHand(clc.currentPlayerID)

	switch promptUser(clc.reader, "Pass or Announce", []string{"P", "T", "GT"}) {
	case 0:
		clc.game.Pass(clc.currentPlayerID)
	case 1:
		clc.game.Announce(clc.currentPlayerID, model.Tichu)
	case 2:
		clc.game.Announce(clc.currentPlayerID, model.GrandTichu)
	default:
		fmt.Printf("Please enter one of the available options")
		return
	}

	clc.currentPlayerID = (clc.currentPlayerID + 1) % 4
}

func (clc *CommandLineClient) promptCardPush() {
	clc.printHand(clc.currentPlayerID)

	cards, _ := promptCards(clc.reader, "Select a card for left, right and opposite player", false)
	if len(cards) != 3 {
		fmt.Printf("Please select 3 cards")
		return
	}
	clc.game.PushCards(clc.currentPlayerID, cards)

	if clc.game.GetPhase() == application.PushingCards {
		clc.currentPlayerID = (clc.currentPlayerID + 1) % 4
	}
}

func (clc *CommandLineClient) promptTrickTurn() {
	clc.currentPlayerID = clc.game.GetNextPlayer()
	comb := clc.game.GetCurrentCombination()
	if comb != nil {
		fmt.Printf("\nLast played combination: %v\n", comb)
	}
	fmt.Printf("Current player: %d\n", clc.currentPlayerID)

	choice := promptUser(clc.reader, "BOMB (by any player)", []string{"0", "1", "2", "3", "N"})
	if choice == 4 {
		clc.printHand(clc.currentPlayerID)
		cardIdx, pass := promptCards(clc.reader, "Pass or play a combination", true)
		if pass {
			clc.game.Pass(clc.currentPlayerID)
		} else {
			clc.game.PlayCombination(clc.currentPlayerID, cardIdx)
		}
	} else if choice >= 0 && choice < 4 {
		clc.printHand(choice)
		cardIdx, _ := promptCards(clc.reader, "Play a bomb", false)
		clc.game.PlayBomb(choice, cardIdx)
	} else {
		fmt.Printf("Please enter one of the available options")
	}

}

func (clc CommandLineClient) printHand(playerID int) {
	hand := clc.game.GetHand(playerID)
	fmt.Printf("Player %d cards: %s\n", playerID, hand)
}

func promptUser(r *bufio.Reader, prompt string, options []string) int {
	fmt.Printf("%s %s: ", prompt, options)

	text, _ := r.ReadString('\n')
	text = strings.TrimSuffix(text, "\n")
	text = strings.TrimSuffix(text, "\r")

	for i, o := range options {
		if o == text || strings.ToLower(o) == text {
			return i
		}
	}

	return -1
}

func promptCards(r *bufio.Reader, prompt string, canPass bool) ([]int, bool) {
	if canPass {
		fmt.Printf("%s [P, 0-n]: ", prompt)
	} else {
		fmt.Printf("%s [0-n]: ", prompt)
	}

	text, _ := r.ReadString('\n')
	text = strings.TrimSuffix(text, "\n")
	text = strings.TrimSuffix(text, "\r")

	if canPass && (text == "P" || text == "p") {
		return nil, true
	}

	cardIndixes := strings.Split(text, " ")
	cardSl := make([]int, len(cardIndixes))
	for i, idx := range cardIndixes {
		intIdx, err := strconv.Atoi(idx)
		if err != nil {
			fmt.Printf("Error: non-integer card index received\n")
			continue
		}
		cardSl[i] = intIdx
	}

	return cardSl, false
}
