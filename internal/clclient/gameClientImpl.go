package clclient

import "fmt"

func (clc *CommandLineClient) CardDealFinished() {

}

func (clc *CommandLineClient) GameFinished() {
	fmt.Printf("GAME OVER\n")
	clc.done = true
}

func (clc *CommandLineClient) RoundStarted() {
	fmt.Printf("-----------------\n")
	fmt.Printf("NEW ROUND STARTED\n")
	fmt.Printf("-----------------\n")
}

func (clc *CommandLineClient) RoundFinished() {
	fmt.Printf("\n")
	fmt.Printf("--------------\n")
	fmt.Printf("ROUND FINISHED\n")
	fmt.Printf("--------------\n\n")
	fmt.Printf("Points:  : \n\n")
}

func (clc *CommandLineClient) CardDealStarted() {
	fmt.Printf("\n")
	fmt.Printf("-------------\n")
	fmt.Printf("DEALING CARDS\n")
	fmt.Printf("-------------\n\n")
	fmt.Printf("Each player can now choose to announce a Tichu or Grand Tichu\n\n")

	clc.currentPlayerID = 0
}

func (clc *CommandLineClient) CardPushStarted() {
	fmt.Printf("\n")
	fmt.Printf("-------------\n")
	fmt.Printf("PUSHING CARDS\n")
	fmt.Printf("-------------\n\n")
	fmt.Printf("Each player now needs to choose a card to push to each other players\n\n")
}

func (clc *CommandLineClient) CardPushFinished() {}

func (clc *CommandLineClient) TrickStarted() {
	fmt.Printf("\n")
	fmt.Printf("---------\n")
	fmt.Printf("NEW TRICK\n")
	fmt.Printf("---------\n\n")
	fmt.Printf("In turn, players now can play a combination\n\n")

	clc.currentPlayerID = clc.game.GetNextPlayer()
}

func (clc *CommandLineClient) TrickFinished() {}

func (clc *CommandLineClient) MahJongPlayed() {
	cardVal, pass := promptCards(clc.reader, "You played the MahJong card. Make a wish for a card value or pass", true)
	if pass {
		clc.game.Pass(clc.currentPlayerID)
	} else {
		// TODO handle errors, especially when the user provided a wrong number of cards
		if len(cardVal) > 0 {
			clc.game.MakeWish(clc.currentPlayerID, cardVal[0])
		}
	}
}

func (clc *CommandLineClient) DogPlayed() {
	fmt.Printf("\n")
	fmt.Printf("----------\n")
	fmt.Printf("DOG PLAYED\n")
	fmt.Printf("----------\n\n")
}
