package model

import (
	"fmt"
)

type PlayingAction interface {
	// TODO should these methods be here or only in roundState?
	// canBePlayed(round *round) bool
	// play(round *round)
	isPlayingAction()
}

type AnnounceAction struct {
	playerID     int
	announcement AnnouncementType
}

func (a AnnounceAction) isPlayingAction() {}

type PushCardsAction struct {
	playerID  int
	toPartner Card
	toLeft    Card
	toRight   Card
}

func (a PushCardsAction) isPlayingAction() {}

type PlayBombAction struct {
	playerID int
	comb     Combination
}

func (a PlayBombAction) isPlayingAction() {}

type PassAction struct {
	playerID int
}

func (a PassAction) isPlayingAction() {}

type PlayCombinationAction struct {
	playerID int
	comb     Combination
}

func (a PlayCombinationAction) isPlayingAction() {}

type WishAction struct {
	playerID int
	wish     CardValue
}

func (a WishAction) isPlayingAction() {}

func NewAnnounceAction(playerID int, announcement AnnouncementType) (*AnnounceAction, error) {
	return &AnnounceAction{playerID: playerID, announcement: announcement}, nil
}

func NewWishAction(playerID int, val CardValue) (*WishAction, error) {
	if val < Two || val > Ace {
		return nil, fmt.Errorf("value must be between %d and %d", Two, Ace)
	}
	return &WishAction{playerID: playerID, wish: val}, nil
}

func NewPushCardsAction(playerID int, toPartner, toLeft, toRight Card) (*PushCardsAction, error) {
	return &PushCardsAction{playerID: playerID, toPartner: toPartner, toLeft: toLeft, toRight: toRight}, nil
}

func NewPlayBombAction(playerID int, cards []Card) (*PlayBombAction, error) {
	comb, err := newCombination(cards)
	if err != nil {
		return nil, err
	}
	if comb.Type < Quartet {
		return nil, fmt.Errorf("Combination is not a bomb")
	}

	return &PlayBombAction{playerID: playerID, comb: *comb}, nil
}

func NewPassAction(playerID int) (*PassAction, error) {
	return &PassAction{playerID: playerID}, nil
}

func NewPlayCombinationAction(playerID int, cards []Card) (*PlayCombinationAction, error) {
	comb, err := newCombination(cards)
	if err != nil {
		return nil, err
	}
	if comb.Type >= Quartet {
		return nil, fmt.Errorf("Combination is not a regular combination, but a bomb")
	}

	return &PlayCombinationAction{playerID: playerID, comb: *comb}, nil
}
