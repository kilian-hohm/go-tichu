package model

import (
	"fmt"
)

// combinationType describes the type of a combination. Bombs are also seen
// as regular combinations
type combinationType int

// Constants to enumerate possible combination types
const (
	Single combinationType = iota
	Pair
	Trio
	FullHouse
	Straight
	Stairs
	Quartet
	StraightFlush
)

var combinationNames = [...]string{"Single", "Pair", "Trio", "FullHouse", "Straight", "Stairs", "Quartet", "StraightFlush"}

func (ct combinationType) String() string {
	if ct < Single || ct > StraightFlush {
		return "Unknown"
	}

	return combinationNames[ct]
}

// combination is a value object that encapsulates all information about
// a combination
type Combination struct {
	Type         combinationType
	HighestValue float32
	SortedCards  []Card
}

// beats determines if the combination is stronger than the combination
// provided as an argument
func (c Combination) beats(c2 Combination) bool {
	if c.Type == c2.Type {
		switch c.Type {
		case Single, Pair, Trio, FullHouse, Quartet:
			return c.HighestValue > c2.HighestValue
		case Straight, StraightFlush, Stairs:
			// Length must be the same, then compare highest card
			return len(c.SortedCards) == len(c2.SortedCards) && c.HighestValue > c2.HighestValue
		}
	} else if c.Type >= Quartet && c2.Type < Quartet {
		// bombs beat all other combinations
		return true
	} else if c.Type == StraightFlush && c2.Type == Quartet {
		// higher bomb beats lower one
		return true
	}

	return false
}

func (c Combination) String() string {
	return fmt.Sprintf("%s(%.1f)", c.Type, c.HighestValue)
}
