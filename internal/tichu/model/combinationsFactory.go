package model

import (
	"errors"
	"sort"
)

var errorNoCombination = errors.New("not a valid combination")

// newCombination tries to create a valid combination from a set of cards.
// If the set of cards does not constitute a valid combination,
// an error is returned.
func newCombination(cards []Card) (*Combination, error) {
	// collect all possible card combinations
	var combinationGenerators []func(c, n, s []Card) (*Combination, error)
	if numCards := len(cards); numCards == 1 {
		combinationGenerators = append(combinationGenerators, newCombinationSingle)
	} else if numCards == 2 {
		combinationGenerators = append(combinationGenerators, newCombinationPair)
	} else if numCards == 3 {
		combinationGenerators = append(combinationGenerators, newCombinationTrio)
	} else if numCards == 4 {
		// Check for quartet
		combinationGenerators = append(combinationGenerators, newCombinationQuartetBomb)
		// Check for stairs
		combinationGenerators = append(combinationGenerators, newCombinationStair)
	} else {
		if numCards == 5 {
			combinationGenerators = append(combinationGenerators, newCombinationFullHouse)
		}
		// Check for straight flush before normal straight
		combinationGenerators = append(combinationGenerators, newCombinationStraightFlush)

		// Check for straight
		combinationGenerators = append(combinationGenerators, newCombinationStraight)

		// Check for stairs
		combinationGenerators = append(combinationGenerators, newCombinationStair)
	}

	// check possible combinations
	sort.Sort(cardSlice(cards))
	normals, specials := separateSpecials(cards)
	for _, generator := range combinationGenerators {
		if comb, err := generator(cards, normals, specials); err == nil {
			return comb, err
		}
	}

	// no combination found
	return nil, errorNoCombination
}

func newCombinationSingle(cards, _, _ []Card) (*Combination, error) {
	if len(cards) == 1 {
		return &Combination{Single, float32(cards[0].value()), cards}, nil
	}
	return nil, errorNoCombination
}

func newCombinationPair(cards, normals, specials []Card) (*Combination, error) {
	if len(cards) == 2 {
		if hasSameValue(cards) || (len(specials) == 1 && specials[0] == Phoenix) {
			return &Combination{Pair, float32(normals[0].value()), cards}, nil
		}
	}
	return nil, errorNoCombination
}

func newCombinationTrio(cards, normals, specials []Card) (*Combination, error) {
	if hasSameValue(cards) || (len(specials) == 1 && specials[0] == Phoenix && hasSameValue(normals)) {
		return &Combination{Trio, float32(normals[0].value()), cards}, nil
	}
	return nil, errorNoCombination
}

func newCombinationQuartetBomb(cards, normals, _ []Card) (*Combination, error) {
	if len(cards) == len(normals) && hasSameValue(cards) {
		return &Combination{Quartet, float32(normals[0].value()), cards}, nil
	}
	return nil, errorNoCombination
}

func newCombinationFullHouse(cards, normals, specials []Card) (*Combination, error) {
	// A B B B P idx:1 l:4 maxIdx:1,2,3
	// A A B B P idx:2 l:4 maxIdx:2,3
	// A A A B P idx:3 l:4 maxIdx:0,1,2
	// A A B B B idx:2 l:5 maxIdx:2,3,4
	// A A A B B idx:3 l:5 maxIdx:0,1,2

	if numSpecials := len(specials); numSpecials == 0 {
		if split := indexOfMaxDiffValue(cards, 0); (split == 2 || split == 3) && hasSameValue(cards[split:5]) {
			return &Combination{FullHouse, float32(cards[5-split].value()), cards}, nil
		}
	} else if numSpecials == 1 && specials[0] == Phoenix {
		if split := indexOfMaxDiffValue(normals, 0); split < 4 && hasSameValue(normals[split:4]) {
			return &Combination{FullHouse, float32(normals[4-split].value()), cards}, nil
		}
	}
	return nil, errorNoCombination
}

func newCombinationStraight(cards, normals, specials []Card) (*Combination, error) {
	var values []int
	phoenix := false
	// prepare straight computation
	for _, c := range specials {
		switch c {
		case Phoenix:
			phoenix = true
		case MahJong:
			values = append(values, 1)
		default:
			return nil, errorNoCombination
		}
	}
	for _, c := range normals {
		values = append(values, int(c.value()))
	}

	// check for a valid stair
	for i := 0; i < len(values)-1; i++ {
		switch values[i+1] - values[i] {
		case 1:
			continue
		case 2:
			// in case the gap is 2 we can use the phoenix once
			if phoenix {
				phoenix = false
			} else {
				return nil, errorNoCombination
			}
		default:
			return nil, errorNoCombination
		}
	}

	// compute the bonus value for the phoenix
	bonus := float32(0)
	if phoenix && len(values) > 0 && values[0] == 1 {
		bonus = 1
	} else if phoenix {
		bonus = 0.5
	}

	return &Combination{
		Type:         Straight,
		HighestValue: float32(normals[len(normals)-1].value()) + bonus,
		SortedCards:  cards,
	}, nil
}

func newCombinationStair(cards, normals, specials []Card) (*Combination, error) {
	// amount of cards has to be even
	if len(cards)%2 != 0 {
		return nil, errorNoCombination
	}

	// check if phoenix is available and return error for any other special card
	phoenix := false
	for _, c := range specials {
		if c == Phoenix {
			phoenix = true
		} else {
			return nil, errorNoCombination
		}
	}

	// do stair computation
	j := 0
	for i := 0; i < len(normals)-1; i++ {
		switch float64(int(normals[i+1].value()-normals[i].value()) - ((i + j) % 2)) {
		case 0:
			continue
		case 1:
			// check if phoenix is available
			if phoenix {
				phoenix = false
				j = 1
			} else {
				return nil, errorNoCombination
			}
		default:
			return nil, errorNoCombination
		}
	}

	return &Combination{
		Type:         Stairs,
		HighestValue: float32(normals[len(normals)-1].value()),
		SortedCards:  cards,
	}, nil
}

func newCombinationStraightFlush(cards, normals, specials []Card) (*Combination, error) {
	if len(specials) != 0 || !hasSameSuit(normals) {
		return nil, errorNoCombination
	}
	comb, err := newCombinationStraight(cards, normals, specials)
	if err == nil {
		comb.Type = StraightFlush
	}
	return comb, err
}

// separateSpecials is a convenience function that separates normal and special cards
// in a slice of cards. It returns two slices with the normal and special cards
func separateSpecials(cards []Card) ([]Card, []Card) {
	normal := make([]Card, 0, len(cards))
	specials := make([]Card, 0, len(cards))
	for _, c := range cards {
		if c < MahJong {
			normal = append(normal, c)
		} else {
			specials = append(specials, c)
		}
	}
	return normal, specials
}

// hasSameValue checks if cards have the same value
func hasSameValue(cards []Card) bool {
	for i := 0; i < len(cards)-1; i++ {
		if cards[i].value() != cards[i+1].value() {
			return false
		}
	}
	return true
}

func indexOfMaxDiffValue(cards []Card, maxDiff int) int {
	for i := 1; i < len(cards); i++ {
		if int(cards[i].value()-cards[i-1].value()) > maxDiff {
			return i
		}
	}
	return 0
}

// hasSameSuit checks if cards have the same suit
func hasSameSuit(cards []Card) bool {
	for i := 0; i < len(cards)-1; i++ {
		if cards[i].suit() != cards[i+1].suit() {
			return false
		}
	}
	return true
}
