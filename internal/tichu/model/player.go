package model

type Player struct {
	anouncement AnnouncementType
	hand        []Card
}

func (p *Player) GetHandByIndex(idx []int) (cards []Card) {
	cards = make([]Card, 0, len(idx))
	for _, i := range idx {
		if i < len(p.hand) {
			cards = append(cards, p.hand[i])
		}
	}

	return
}

func (p *Player) GetHand() []Card {
	return p.hand
}

func (p *Player) playCards(c []Card) {
	p.hand = cardSlice(p.hand).Remove(c)
}

func (p Player) isOutOfcards() bool {
	return len(p.hand) == 0
}

func nextPlayerID(startID int, Players [4]Player) int {
	startID = (startID + 1) % 4
	for Players[startID].isOutOfcards() {
		startID = (startID + 1) % 4
	}
	return startID
}
