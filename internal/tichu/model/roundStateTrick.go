package model

import "log"

// roundStateTrick is one of the possible states a round can have. During the
// roundStateTrick phase, players can play combinations, bombs or pass. A trick
// ends when three players pass in a row. Upon leaving, the points for this trick
// are accumulated.
type RoundStateTrick struct {
	currentPlayerID        int
	currentHighestPlayerID int
	passCount              int
	actions                []PlayingAction
	lastAction             PlayingAction
	parent                 *Round
	points                 int
	waitingForWish         bool
}

func newRoundStateTrick(Round *Round, startingPlayer int) *RoundStateTrick {
	log.Printf("New Trick, starting player: %d\n", startingPlayer)
	return &RoundStateTrick{currentPlayerID: startingPlayer, passCount: 0, parent: Round}
}

func (t RoundStateTrick) GetNextPlayer() int {
	return t.currentPlayerID
}

func (t *RoundStateTrick) enter() {}
func (t *RoundStateTrick) leave() {
	for _, pa := range t.actions {
		switch a := pa.(type) {
		case PlayCombinationAction:
			for _, c := range a.comb.SortedCards {
				t.points += c.points()
			}
		case PlayBombAction:
			for _, c := range a.comb.SortedCards {
				t.points += c.points()
			}
		}
	}
}
func (t RoundStateTrick) checkCondition() bool {
	if t.passCount == 3 {
		return true
	} else if a, ok := t.lastAction.(*PlayCombinationAction); ok && a.comb.Type == Single && cardSlice(a.comb.SortedCards).Has(Dog) {
		return true
	}
	return false
}

func (t *RoundStateTrick) play(pa PlayingAction) {
	t.actions = append(t.actions, pa)
	switch a := pa.(type) {
	case *PassAction:
		t.passCount++
		if t.waitingForWish {
			t.waitingForWish = false
		}
	case *PlayCombinationAction:
		// TODO: wishing a card when mahjong is played
		t.passCount = 0
		if a.comb.Type == Single && a.comb.SortedCards[0] == Dog {
			// +1 because nextPlayerID also increments by +1, so this makes the first player to
			//try the opposite one
			t.currentHighestPlayerID = nextPlayerID((a.playerID+1)%4, t.parent.Players)
		} else {
			t.currentHighestPlayerID = a.playerID
		}
		t.parent.Players[a.playerID].playCards(a.comb.SortedCards)
		// If the MahJong card was played, the same player must make a wish or pass
		if cardSlice(a.comb.SortedCards).Has(MahJong) {
			t.waitingForWish = true
			// TODO calling this directly feels wrong
			t.parent.parent.client.MahJongPlayed()
		}
		if t.parent.Players[a.playerID].isOutOfcards() {
			t.parent.playersOutOfCards = append(t.parent.playersOutOfCards, a.playerID)
		}
	case *PlayBombAction:
		t.passCount = 0
		t.currentHighestPlayerID = a.playerID
		t.parent.Players[a.playerID].playCards(a.comb.SortedCards)
		if t.parent.Players[a.playerID].isOutOfcards() {
			t.parent.playersOutOfCards = append(t.parent.playersOutOfCards, a.playerID)
		}
	case *AnnounceAction:
		t.parent.Players[a.playerID].anouncement = a.announcement
	case *WishAction:
		t.waitingForWish = false
	}
	t.currentPlayerID = nextPlayerID(t.currentPlayerID, t.parent.Players)
	t.lastAction = pa
}

func (t RoundStateTrick) isValid(pa PlayingAction) bool {
	switch a := pa.(type) {
	case *PassAction:
		// Passing is always possible, as long as the current player does it
		return t.currentPlayerID == a.playerID
	case *PlayCombinationAction:
		// Playing a combination is valid if the current player does it, and if the
		// combination is higher than the current one
		if t.waitingForWish {
			return false
		}
		playersTurn := t.currentPlayerID == a.playerID
		if len(t.actions) > 0 {
			switch la := t.lastAction.(type) {
			case PushCardsAction, PlayBombAction:
				return false
			case PassAction:
				return playersTurn
			case PlayCombinationAction:
				return playersTurn && a.comb.beats(la.comb)
			}
		}
		return playersTurn
	case *PlayBombAction:
		if t.waitingForWish {
			return false
		}
		// Bombing is always allowed, except when the current played card is the Dog
		if len(t.actions) == 1 {
			if la, ok := t.actions[0].(PlayCombinationAction); ok {
				if la.comb.Type == Single && cardSlice(la.comb.SortedCards).Has(Dog) {
					return false
				}
			}
		}
		return true
	case *AnnounceAction:
		// Players can announce Tichu until they have played their first card
		// regardless if its their turn or not
		return a.announcement == Tichu && len(t.parent.Players[a.playerID].hand) == 14
	case *WishAction:
		la, ok := t.lastAction.(*PlayCombinationAction)
		return t.waitingForWish && ok && la.playerID == a.playerID
	default:
		return false
	}
}
