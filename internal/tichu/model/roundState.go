package model

// roundState is used to implement the state pattern that is used by
// round to determine currently possible actions and how to play them
type roundState interface {
	// enter gets called when transitioning into the state
	enter()
	// leave gets called when transitioning out of the state
	leave()
	// checkCondition returns true when the state is done and the transition
	// to the next state should happen
	checkCondition() bool
	// play performs the playingAction
	play(PlayingAction)
	// isValid checks if the playingAction is allowed to be played for the state
	isValid(PlayingAction) bool
	GetNextPlayer() int
}
