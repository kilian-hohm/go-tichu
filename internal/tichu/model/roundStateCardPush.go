package model

import (
	"log"
	"sort"
)

// roundStateCardPush is one of the possible states a round can have. During this state
// all players need to select one card to pass to each other player. Once this is done,
// cards are exchanged and the state is finished.
type RoundStateCardPush struct {
	actions [4]*PushCardsAction
	parent  *Round
}

func newRoundStateCardPush(Round *Round) *RoundStateCardPush {
	return &RoundStateCardPush{actions: [4]*PushCardsAction{nil, nil, nil, nil}, parent: Round}
}

func (cp RoundStateCardPush) GetNextPlayer() int {
	return -1
}

func (cp *RoundStateCardPush) enter() {
}

func (cp *RoundStateCardPush) leave() {
	var (
		receive [4][]Card
		give    [4][]Card
	)
	for i := 0; i < 4; i++ {
		give[i] = []Card{cp.actions[i].toLeft, cp.actions[i].toPartner, cp.actions[i].toRight}

		receive[i] = make([]Card, 3)
		receive[i][0] = cp.actions[(i+1)%4].toLeft
		receive[i][1] = cp.actions[(i+2)%4].toPartner
		receive[i][2] = cp.actions[(i+3)%4].toRight
	}

	for i := 0; i < 4; i++ {
		cp.parent.Players[i].hand = cardSlice(cp.parent.Players[i].hand).Remove(give[i]).Add(receive[i])
		sort.Sort(cardSlice(cp.parent.Players[i].hand))
	}
}

func (cp RoundStateCardPush) checkCondition() bool {
	return cp.actions[0] != nil && cp.actions[1] != nil && cp.actions[2] != nil && cp.actions[3] != nil
}

func (cp *RoundStateCardPush) play(pa PlayingAction) {
	switch a := pa.(type) {
	case *PushCardsAction:
		cp.actions[a.playerID] = a
	case *AnnounceAction:
		cp.parent.Players[a.playerID].anouncement = a.announcement
	default:
		// This should not happen, since an actions validity should be checked
		// before calling play
		log.Println("ERROR: Invalid action being applied")
	}
}

func (cp RoundStateCardPush) isValid(pa PlayingAction) bool {
	switch a := pa.(type) {
	case *PushCardsAction:
		// During the roundStateCardPush state, only pushing cards is a valid action. However,
		// each player can only play one action
		return cp.actions[a.playerID] == nil
	case *AnnounceAction:
		// Players can announce Tichu until they have played their first card
		// regardless if its their turn or not
		return a.announcement == Tichu && len(cp.parent.Players[a.playerID].hand) == 14
	default:
		return false
	}
}
