package model

// TichuGameClient is the interface a client has to implement.
// The interface functions are used to signal state changes in
// the game to the client.
type TichuGameClient interface {
	// GameFinished is called when a Tichu game has finished.
	GameFinished()
	// RoundStarted is called when a new round of a game
	// has started.
	RoundStarted()
	// RoundFinished is called when a round of the game
	// has finished.
	RoundFinished()
	// CardDealStarted is called when the phase of dealing
	// the cards of a round has started. At this point the first
	// 8 cards have been dealt and players can announce a
	// Grand Tichu if they want to.
	CardDealStarted()
	// CardDealFinished is called when all cards have been dealt.
	CardDealFinished()
	// CardPushStarted is called when the phase of pushing cards to
	// all other players has started.
	CardPushStarted()
	// CardPushFinished is called when the phase of pushing cards
	// is finished.
	CardPushFinished()
	// TrickStarted is called when a new trick has been started.
	TrickStarted()
	// TrickFinished is called when a trick is finshed.
	TrickFinished()
	// MahJongPlayed is called when a player plays the MahJong card.
	// The same player must then make a wish for a card that has to
	// be played.
	MahJongPlayed()
	// DogPlayed is called when a player plays the Dog
	DogPlayed()
}
