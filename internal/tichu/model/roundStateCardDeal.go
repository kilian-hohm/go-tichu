package model

// roundStateCardDeal is the phase of the game where the cards are dealt.
// After receiving 8 of the 14 cards players have the option of announcing a
// "Grand Tichu" (or a regular one), or pass. The state is done when all players
// have made their choice. When leaving, the remaining 6 cards are dealt to all players.
type RoundStateCardDeal struct {
	// anouncements should be moved to a player class?
	playerDone [4]bool
	parent     *Round
}

func newRoundStateCardDeal(r *Round) *RoundStateCardDeal {
	s := &RoundStateCardDeal{parent: r}
	return s
}

func (rs RoundStateCardDeal) GetNextPlayer() int {
	return -1
}

func (rs *RoundStateCardDeal) enter() {
	// It feels awkward to call the parents method at this point
	// But dealing cards should be the job of this state
	rs.parent.dealCards(0, 8)
}

func (rs *RoundStateCardDeal) leave() {
	rs.parent.dealCards(8, 14)
}

func (rs *RoundStateCardDeal) checkCondition() bool {
	return rs.playerDone[0] && rs.playerDone[1] && rs.playerDone[2] && rs.playerDone[3]
}

func (rs RoundStateCardDeal) isValid(pa PlayingAction) bool {
	if _, ok := pa.(*PassAction); ok {
		return true
	} else if _, ok := pa.(*AnnounceAction); ok {
		return true
	}
	return false
}

func (rs *RoundStateCardDeal) play(pa PlayingAction) {
	if aa, ok := pa.(*AnnounceAction); ok {
		rs.parent.Players[aa.playerID].anouncement = aa.announcement
		rs.playerDone[aa.playerID] = true
	} else if pa, ok := pa.(*PassAction); ok {
		rs.parent.Players[pa.playerID].anouncement = None
		rs.playerDone[pa.playerID] = true
	}
}
