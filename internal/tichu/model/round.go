package model

import (
	"math/rand"
	"sort"
)

type Round struct {
	CurrentState      roundState
	stateHistory      []roundState
	ShuffledCards     [56]Card
	Players           [4]Player
	playersOutOfCards []int
	parent            *TichuGame
}

func NewRound(p *TichuGame) *Round {
	r := Round{}
	r.parent = p
	for c, idx := range rand.Perm(56) {
		r.ShuffledCards[idx] = Card(c)
	}
	r.CurrentState = newRoundStateCardDeal(&r)
	r.CurrentState.enter()
	r.playersOutOfCards = make([]int, 0, 4)
	return &r
}

func (r *Round) Play(pa PlayingAction) {
	r.CurrentState.play(pa)
	if r.checkForEnd() || r.CurrentState.checkCondition() {
		r.nextState()
	}
}

func (r *Round) IsValid(pa PlayingAction) bool {
	return r.CurrentState.isValid(pa)
}

func (r *Round) countPoints() (int, int) {
	winnerTeam := r.playersOutOfCards[0] % 2
	winnerPoints := 0
	loserPoints := 0
	// Check if anybody announced (gr.) Tichu
	for i, p := range r.Players {
		if p.anouncement != None {
			if i == r.playersOutOfCards[0] {
				winnerPoints += p.anouncement.points()
			} else {
				if i%2 == winnerTeam {
					winnerPoints -= p.anouncement.points()
				} else {
					loserPoints -= p.anouncement.points()
				}
			}
		}
	}
	if len(r.playersOutOfCards) == 2 {
		winnerPoints += 200
	} else {
		lastPlayer := 6 - r.playersOutOfCards[0] - r.playersOutOfCards[1] - r.playersOutOfCards[2]
		// The last player gives his hand cards to the opposing team
		winnerPoints += cardSlice(r.Players[lastPlayer].hand).points()

		// Go through all tricks and summ the points
		for _, s := range r.stateHistory {
			if t, ok := s.(*RoundStateTrick); ok {
				if t.currentHighestPlayerID == lastPlayer {
					// All tricks won by the lastPlayer are given to the team of the player who was out of cards first
					if r.playersOutOfCards[0]%2 == winnerTeam {
						winnerPoints += t.points
					} else {
						loserPoints += t.points
					}
				} else if t.currentHighestPlayerID%2 == winnerTeam {
					winnerPoints += t.points
				} else {
					loserPoints += t.points
				}
			}
		}
	}

	if winnerTeam == 0 {
		return winnerPoints, loserPoints
	}
	return loserPoints, winnerPoints
}

func (r *Round) nextState() {
	r.CurrentState.leave()
	r.stateHistory = append(r.stateHistory, r.CurrentState)
	if r.checkForEnd() {
		r.CurrentState = newRoundStateDone(r)
	} else {
		switch lastState := r.CurrentState.(type) {
		case *RoundStateCardDeal:
			r.CurrentState = newRoundStateCardPush(r)
			r.parent.client.CardPushStarted()
		case *RoundStateCardPush:
			startingPlayer := -1
			for i, p := range r.Players {
				if cardSlice(p.hand).Has(MahJong) {
					startingPlayer = i
					break
				}
			}
			r.CurrentState = newRoundStateTrick(r, startingPlayer)
			r.parent.client.TrickStarted()
		case *RoundStateTrick:
			r.CurrentState = newRoundStateTrick(r, lastState.currentHighestPlayerID)
			r.parent.client.TrickStarted()
		}
	}
	r.CurrentState.enter()
}

func (r *Round) dealCards(from, to int) {
	for i := 0; i < 4; i++ {
		r.Players[i].hand = cardSlice(r.Players[i].hand).Add(r.ShuffledCards[i*14+from : i*14+to])
		sort.Sort(cardSlice(r.Players[i].hand))
	}
}

func (r *Round) checkForEnd() bool {
	if numOOCards := len(r.playersOutOfCards); numOOCards == 2 {
		return (r.playersOutOfCards[0]+r.playersOutOfCards[1])%2 == 0
	} else if numOOCards == 3 {
		return true
	}
	return false
}
