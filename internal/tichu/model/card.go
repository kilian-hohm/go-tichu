package model

import "fmt"

// card represents a playing card of the 56 cards deck.
//
// The first 52 cards consist of the regular cards, divided into 4 "suits" of 13 cards each.
// Therefore, the suit of a card can be determined by integer division by 13
// and the value of the card by mod 13.
//
// Special cards are: MahJong #52, Dragon #53, Dog #54, Phoenix #55
type Card int

// Constants enumerating the special cardss
const (
	MahJong Card = iota + 52
	Dragon
	Dog
	Phoenix
)

var specialNames = [...]string{"MahJong", "Dragon", "Dog", "Phoenix"}

// value returns the value of a card. For the two special cards Dog and Phoenix
// that either have no value, or where the value depends on the context, -1 is
// returned
func (c Card) value() CardValue {
	v := CardValue(-1)

	s := c / 13
	if s < 4 {
		// If suit is not "special", the value is (cardID mod 13) + 2
		m := int(c % 13)
		v = CardValue(m + 2)
	} else {
		// Of the special cards only MahJong and Dragon have a fixed value
		if c == MahJong {
			v = 1
		} else if c == Dragon {
			v = 15 // Ace + 1
		} else if c == Phoenix {
			v = 0 // Last cards value + 0.5 cannot be represented
		}
	}

	return v
}

// suit returns the suit of a card. Possible values are defined in as constants
func (c Card) suit() cardSuit {
	return cardSuit(c / 13)
}

func (c Card) points() int {
	if c == Dragon {
		return 25
	} else if c == Phoenix {
		return -25
	} else if value := c.value(); value == Five {
		return 5
	} else if value == Ten || value == King {
		return 10
	}
	return 0
}

func (c Card) String() string {
	s := "Unknown"

	if suit := c.suit(); suit != Special {
		s = fmt.Sprintf("%s-%s", suit, c.value())
	} else {
		if c <= Phoenix {
			s = specialNames[c-52]
		}
	}

	return s
}
