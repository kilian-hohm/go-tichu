package model

type TichuGame struct {
	CurrentRound *Round
	roundHistory []*Round
	TeamPoints   [2]int
	client       TichuGameClient
}

func NewTichuGame(client TichuGameClient) *TichuGame {
	g := TichuGame{}
	g.client = client
	g.client.RoundStarted()
	g.client.CardDealStarted()
	g.CurrentRound = NewRound(&g)

	return &g
}

func (g TichuGame) IsValid(pa PlayingAction) bool {
	return g.CurrentRound.IsValid(pa)
}

func (g *TichuGame) Play(pa PlayingAction) {
	g.CurrentRound.Play(pa)
	if g.CurrentRound.checkForEnd() {
		g.client.RoundFinished()
		g.next()
	}
}

func (g TichuGame) GetCurrentCombination() *Combination {
	if trick, ok := g.CurrentRound.CurrentState.(*RoundStateTrick); ok {
		for i := len(trick.actions) - 1; i >= 0; i-- {
			switch a := trick.actions[i].(type) {
			case *PlayBombAction:
				return &a.comb
			case *PlayCombinationAction:
				return &a.comb
			}
		}
	}
	return nil
}

func (g TichuGame) checkForEnd() bool {
	return g.TeamPoints[0] >= 1000 || g.TeamPoints[1] >= 1000
}

func (g *TichuGame) next() {
	p1, p2 := g.CurrentRound.countPoints()
	g.TeamPoints[0] += p1
	g.TeamPoints[1] += p2

	g.roundHistory = append(g.roundHistory, g.CurrentRound)

	if g.checkForEnd() {
		g.CurrentRound = nil
		g.client.GameFinished()
	} else {
		g.CurrentRound = NewRound(g)
		g.client.RoundStarted()
	}
}
