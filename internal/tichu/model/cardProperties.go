package model

// cardSuit describes the suit of a card, or if it is one of the special cards
type cardSuit int

// Constants enumerating possible suit values of cards
const (
	Red cardSuit = iota
	Green
	Blue
	Yellow
	Special
)

var cardSuitNames = [...]string{
	"red",
	"green",
	"blue",
	"yellow",
	"special",
}

func (s cardSuit) String() string {

	if s < Red || s > Special {
		return "Unknown"
	}

	return cardSuitNames[s]
}

// cardValue describes the value of a card that can be used for sorting and comparing
// cards.
type CardValue int

// Constants enumerating values of regular, i.e. non-special, cards
const (
	Two CardValue = iota + 2
	Three
	Four
	Five
	Six
	Seven
	Eight
	Nine
	Ten
	Jack
	Queen
	King
	Ace
)

var cardValueNames = [...]string{
	"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A",
}

func (v CardValue) String() string {
	if v < Two || v > Ace {
		return "Unknown"
	}
	return cardValueNames[v-2]
}
