package model

// announcementType describes the type of a round announcement, i.e. "Tichu" or "Grand Tichu"
type AnnouncementType int

// Constants to enumerate possible announcementType values
const (
	None AnnouncementType = iota
	Tichu
	GrandTichu
)

var announcementNames = [...]string{"None", "Tichu", "Grand Tichu"}

func (m AnnouncementType) String() string {
	if m < None || m > GrandTichu {
		return "Unknown"
	}

	return announcementNames[m]
}

func (m AnnouncementType) points() int {
	switch m {
	case Tichu:
		return 100
	case GrandTichu:
		return 200
	}
	return 0
}
