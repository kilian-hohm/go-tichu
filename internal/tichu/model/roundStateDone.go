package model

// roundStateDone is the phase of the game where the cards are dealt.
// After receiving 8 of the 14 cards players have the option of announcing a
// "Grand Tichu" (or a regular one), or pass. The state is done when all players
// have made their choice. When leaving, the remaining 6 cards are dealt to all players.
type RoundStateDone struct {
}

func newRoundStateDone(_ *Round) RoundStateDone {
	return RoundStateDone{}
}

func (rs RoundStateDone) GetNextPlayer() int {
	return -1
}

func (rs RoundStateDone) enter() {}

func (rs RoundStateDone) leave() {}

func (rs RoundStateDone) checkCondition() bool {
	return false
}

func (rs RoundStateDone) isValid(pa PlayingAction) bool {
	return false
}

func (rs RoundStateDone) play(pa PlayingAction) {}
