package model

// cardSlice is a slice of cards. It implements sort.Interface to sort a slice or cards
// in increasing order of the cards values. cardSlice is also closed under operations
// Add and Remove
type cardSlice []Card

func (p cardSlice) Len() int {
	return len(p)
}

func (p cardSlice) Less(i, j int) bool {
	return p[i].value() < p[j].value()
}

func (p cardSlice) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}

func (p cardSlice) Add(others cardSlice) cardSlice {
	return append(p, others...)
}

func (p cardSlice) Remove(others cardSlice) cardSlice {
	res := make(cardSlice, 0, len(p))
	for _, c := range p {
		keep := true
		for _, rem := range others {
			if rem == c {
				keep = false
				break
			}
		}
		if keep {
			res = append(res, c)
		}
	}

	return res
}

func (p cardSlice) Has(c Card) bool {
	for _, cc := range p {
		if cc == c {
			return true
		}
	}
	return false
}

func (p cardSlice) points() int {
	res := 0
	for _, c := range p {
		res += c.points()
	}
	return res
}
