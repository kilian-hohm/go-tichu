package model

import (
	"fmt"
	"testing"
)

func TestCombinations(t *testing.T) {
	testCases := []struct {
		cards         []Card
		expectedType  combinationType
		expectedValue float32
	}{
		// Singles
		{[]Card{12}, Single, float32(Card(12).value())},
		{[]Card{MahJong}, Single, float32(Card(MahJong).value())},
		{[]Card{Phoenix}, Single, float32(Card(Phoenix).value())},
		// Pairs
		{[]Card{0, 13}, Pair, float32(Card(13).value())},
		{[]Card{27, Phoenix}, Pair, float32(Card(27).value())},
		// Trios
		{[]Card{0, 13, 26}, Trio, float32(Card(0).value())},
		{[]Card{0, Phoenix, 26}, Trio, float32(Card(0).value())},
		// Straights
		{[]Card{13, 1, 2, 3, 4}, Straight, float32(Card(4).value())},
		{[]Card{13, 1, 2, 3, 4, 5}, Straight, float32(Card(5).value())},
		{[]Card{13, 1, 2, 3, 4, 5, 6}, Straight, float32(Card(6).value())},
		{[]Card{MahJong, 13, 1, 2, 3, 4, 5, 6}, Straight, float32(Card(6).value())},
		{[]Card{Phoenix, 1, 2, 3, 4, 5, 6}, Straight, float32(Card(6).value()) + 0.5},
		{[]Card{MahJong, 13, 1, Phoenix, 3, 4, 5, 6}, Straight, float32(Card(6).value())},
		{[]Card{MahJong, 13, 1, 2, 3, 4, 5, Phoenix}, Straight, float32(Card(6).value())},
		{[]Card{13, 1, 2, Phoenix, 4, 5, 6}, Straight, float32(Card(6).value())},
		{[]Card{0, 1, Phoenix, 3, 4}, Straight, float32(Card(4).value())},
		{[]Card{Phoenix, 1, 2, 3, 4}, Straight, float32(Card(4).value()) + 0.5},
		{[]Card{MahJong, 0, 1, 2, 3}, Straight, float32(Card(3).value())},
		{[]Card{MahJong, 0, 1, 2, Phoenix}, Straight, float32(Card(3).value())},
		{[]Card{MahJong, 0, 1, Phoenix, 3}, Straight, float32(Card(3).value())},
		// FullHouses
		{[]Card{0, 13, 26, 1, 14}, FullHouse, float32(Card(0).value())},
		{[]Card{Phoenix, 13, 26, 1, 14}, FullHouse, float32(Card(1).value())},
		{[]Card{0, 13, 26, Phoenix, 14}, FullHouse, float32(Card(0).value())},
		// Stairs
		{[]Card{0, 13, 27, 40}, Stairs, float32(Card(40).value())},
		{[]Card{0, 13, 27, Phoenix}, Stairs, float32(Card(27).value())},
		{[]Card{0, 13, 1, 14, 28, 41}, Stairs, float32(Card(41).value())},
		{[]Card{0, 13, 1, 14, 28, 41, 29, 42}, Stairs, float32(Card(42).value())},
		{[]Card{0, 13, 1, Phoenix, 28, 41, 29, 42}, Stairs, float32(Card(42).value())},
		{[]Card{0, 13, Phoenix, 1, 28, 41, 29, 42}, Stairs, float32(Card(42).value())},
		{[]Card{Phoenix, 13, 14, 1, 28, 41, 29, 42}, Stairs, float32(Card(42).value())},
		{[]Card{0, Phoenix, 14, 1, 28, 41, 29, 42}, Stairs, float32(Card(42).value())},
		{[]Card{0, 13, 14, 1, 28, 41, 29, Phoenix}, Stairs, float32(Card(42).value())},
		// Quartets
		{[]Card{0, 13, 26, 39}, Quartet, float32(Card(39).value())},
		// StraightFlushs
		{[]Card{0, 1, 2, 3, 4}, StraightFlush, float32(Card(4).value())},
		{[]Card{0, 1, 2, 3, 4, 5}, StraightFlush, float32(Card(5).value())},
		{[]Card{0, 1, 2, 3, 4, 5, 6}, StraightFlush, float32(Card(6).value())},
		{[]Card{0, 1, 2, 3, 4, 5, 6, 7}, StraightFlush, float32(Card(7).value())},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%s %v", tc.expectedType, tc.cards), func(t *testing.T) {
			if got, err := newCombination(tc.cards); err != nil {
				t.Errorf("Did not expect an error. but got: %v", err)
			} else if got.Type != tc.expectedType {
				t.Errorf("Wrong combinationType %d, expected %d", got.Type, tc.expectedType)
			} else if got.HighestValue != tc.expectedValue {
				t.Errorf("Wrong HighestValue %f, expected %f", got.HighestValue, tc.expectedValue)
			}
		})
	}
}

func TestCombinationsErrors(t *testing.T) {
	testCases := []struct {
		cards      []Card
		testedType combinationType
	}{
		// Pairs
		{[]Card{0, 3}, Pair},
		{[]Card{0, 3, 13}, Trio},
		{[]Card{0, 13, 26, Phoenix}, Stairs},
		{[]Card{0, 15, 4, 6, Phoenix}, Straight}, // error because multiple gaps of size 1
		{[]Card{0, Phoenix, 14, 1, 28, 41, 30, 43}, Stairs},
		{[]Card{0, Phoenix, 15, 2}, Stairs},
		{[]Card{0, 13, 15, 2}, Stairs},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%s %v", tc.testedType, tc.cards), func(t *testing.T) {
			if comb, err := newCombination(tc.cards); err == nil {
				t.Errorf("Expected an error but got a valid combination: %v", comb)
			}
		})
	}
}

func TestCombinationsComparison(t *testing.T) {
	testCases := []struct {
		c1        Combination
		c2        Combination
		c1BeatsC2 bool
	}{
		{Combination{Type: Single, HighestValue: 5}, Combination{Type: Single, HighestValue: 6}, false},
		{Combination{Type: Single, HighestValue: 5}, Combination{Type: Pair, HighestValue: 6}, false},
		{Combination{Type: Quartet, HighestValue: 2}, Combination{Type: Single, HighestValue: 6}, true},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%s beats %s %t", tc.c1, tc.c2, tc.c1BeatsC2), func(t *testing.T) {
			if got := tc.c1.beats(tc.c2); got != tc.c1BeatsC2 {
				t.Errorf("Expected c1 to beat to c2")
			}
		})
	}
}
