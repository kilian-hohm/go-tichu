package application

// GamePhase is an enum describing the possible phases a
// game can have.
type GamePhase int

// Possible values for the GamePhase
const (
	DealingCards GamePhase = iota
	PushingCards
	Trick
	Done
)

var gamePhaseNames = [...]string{"Dealing Cards", "Pushing Cards", "Trick", "Done"}

func (gp GamePhase) String() string {
	s := "Unknown"

	if gp >= DealingCards && gp <= Done {
		s = gamePhaseNames[gp]
	}

	return s
}
