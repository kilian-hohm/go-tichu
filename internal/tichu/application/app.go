package application

import (
	"fmt"

	"gitlab.com/kilian-hohm/go-tichu/internal/tichu/model"
)

type TichuGame struct {
	game *model.TichuGame
}

func NewTichuGame(client model.TichuGameClient) *TichuGame {
	g := TichuGame{}
	g.game = model.NewTichuGame(client)

	return &g
}

func (g TichuGame) GetPhase() GamePhase {
	if _, ok := g.game.CurrentRound.CurrentState.(*model.RoundStateCardDeal); ok {
		return DealingCards
	} else if _, ok := g.game.CurrentRound.CurrentState.(*model.RoundStateCardPush); ok {
		return PushingCards
	} else if _, ok := g.game.CurrentRound.CurrentState.(*model.RoundStateTrick); ok {
		return Trick
	} else if _, ok := g.game.CurrentRound.CurrentState.(*model.RoundStateDone); ok {
		return Done
	}
	return -1
}

func (g TichuGame) GetHand(playerID int) []model.Card {
	return g.game.CurrentRound.Players[playerID].GetHand()
}

// GetNextPlayer gives you the index of the player in turn.
// If all players are in turn, -1 is returned
func (g TichuGame) GetNextPlayer() int {
	return g.game.CurrentRound.CurrentState.GetNextPlayer()
}

func (g TichuGame) GetCurrentCombination() *model.Combination {
	return g.game.GetCurrentCombination()
}

func (g *TichuGame) Announce(playerID int, announce model.AnnouncementType) error {
	pa, err := model.NewAnnounceAction(playerID, announce)
	if err != nil {
		return err
	}

	return g.playAction(pa)
}

func (g *TichuGame) MakeWish(playerID, value int) error {
	pa, err := model.NewWishAction(playerID, model.CardValue(value))
	if err != nil {
		return err
	}

	return g.playAction(pa)
}

func (g *TichuGame) Pass(playerID int) error {
	pa, err := model.NewPassAction(playerID)
	if err != nil {
		return err
	}

	return g.playAction(pa)
}

func (g *TichuGame) PushCards(playerID int, cardIdx []int) error {
	cards, err := getCardsByIndex(g.GetHand(playerID), cardIdx)
	if err != nil {
		return err
	}
	if len(cards) != 3 {
		return fmt.Errorf("need to provide 3 cards")
	}
	pa, err := model.NewPushCardsAction(playerID, cards[1], cards[0], cards[2])
	if err != nil {
		return err
	}

	return g.playAction(pa)
}

func (g *TichuGame) PlayCombination(playerID int, cardIdx []int) error {
	cards, err := getCardsByIndex(g.GetHand(playerID), cardIdx)
	if err != nil {
		return err
	}
	pa, err := model.NewPlayCombinationAction(playerID, cards)
	if err != nil {
		return err
	}

	return g.playAction(pa)
}

func (g *TichuGame) PlayBomb(playerID int, cardIdx []int) error {
	cards, err := getCardsByIndex(g.GetHand(playerID), cardIdx)
	if err != nil {
		return err
	}
	pa, err := model.NewPlayBombAction(playerID, cards)
	if err != nil {
		return err
	}

	return g.playAction(pa)
}

func getCardsByIndex(hand []model.Card, cardIdx []int) ([]model.Card, error) {
	cards := make([]model.Card, 0, len(cardIdx))
	for _, ci := range cardIdx {
		if ci >= 0 && ci < len(hand) {
			cards = append(cards, hand[ci])
		} else {
			return nil, fmt.Errorf("card index out of bounds")
		}
	}
	return cards, nil
}

func (g *TichuGame) playAction(pa model.PlayingAction) error {
	if g.game.IsValid(pa) {
		g.game.Play(pa)
		return nil
	}
	return fmt.Errorf("action not playable")
}
