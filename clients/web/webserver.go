package main

import (
	"io"
	"net/http"
	"os"
)

func main() {
	fs := http.FileServer(http.Dir("./static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	http.HandleFunc("/", start)
	http.HandleFunc("/game/new", newGame)

	http.ListenAndServe(":8080", nil)
}

func newGame(writer http.ResponseWriter, request *http.Request) {
	serveHTMLFile("templates/newgame.html", writer, request)
}

func start(writer http.ResponseWriter, request *http.Request) {
	serveHTMLFile("templates/start.html", writer, request)
}

func serveHTMLFile(path string, writer http.ResponseWriter, request *http.Request) {
	file, err := os.Open(path)
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer file.Close()
	_, err = io.Copy(writer, file)
	if err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
		return
	}
}
