package main

import "gitlab.com/kilian-hohm/go-tichu/internal/clclient"

func main() {
	clc := clclient.NewCommandLineClient()
	keepGoing := clc.NextPrompt()
	for keepGoing {
		keepGoing = clc.NextPrompt()
	}
}
